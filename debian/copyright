Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Aircrack-ng
Upstream-Contact: Thomas d'Otreppe <tdotreppe@aircrack-ng.org>
Source: https://www.aircrack-ng.org/downloads.html

Files: *
Copyright: 2006-2018 Thomas d'Otreppe <tdotreppe@aircrack-ng.org>
License: GPL-2+

Files: Makefile.am autogen.sh build/m4/*.m4
 configure.ac manpages/Makefile.am scripts/Makefile.am
 scripts/airdrop-ng/Makefile.am scripts/airdrop-ng/doc/Makefile.am
 scripts/airgraph-ng/Makefile.am scripts/airgraph-ng/man/Makefile.am
 scripts/versuck-ng/Makefile.am src/Makefile.am src/include/Makefile.am
 src/aircrack-osdep/Makefile.am src/aircrack-osdep/radiotap/Makefile.am test/Makefile.am
 test/cryptounittest/Makefile.am
Copyright: 2017 Joseph Benden <joe@benden.us>
License: GPL-2+ with OpenSSL exception

Files: build/m4/ax_add_fortify_source.m4
Copyright: 2017 David Seifert <soap@gentoo.org>
License: FSFAP

Files: build/m4/ax_check_openssl.m4
Copyright: 2009,2010 Zmanda Inc. <http://www.zmanda.com/>
           2009,2010 Dustin J. Mitchell <dustin@zmanda.com>
License: FSFAP

Files: build/m4/ax_compare_version.m4
Copyright: 2008 Tim Toolan <toolan@ele.uri.edu>
License: FSFAP

Files: build/m4/ax_compiler_version.m4
Copyright: 2014 Bastien ROUCARIES <roucaries.bastien+autoconf@gmail.com>
License: FSFAP

Files: build/m4/ax_lib_gcrypt.m4
Copyright: 2009 Fabien Coelho <autoconf.archive@coelho.net>
License: FSFAP

Files: build/m4/ax_lib_socket_nsl.m4
Copyright: 2008 Russ Allbery <rra@stanford.edu>
           2008 Stepan Kasal <kasal@ucw.cz>
	   2008 Warren Young <warren@etr-usa.com>
License: FSFAP

Files: build/m4/ax_lib_sqlite3.m4
Copyright: 2008 Mateusz Loskot <mateusz@loskot.net>
License: FSFAP

Files: build/m4/ax_require_defined.m4
Copyright: 2014 Mike Frysinger <vapier@gentoo.org>
License: FSFAP

Files: build/m4/ax_valgrind_check.m4
Copyright: 2014, 2015, 2016 Philip Withnall <philip.withnall@collabora.co.uk>
License: FSFAP

Files: build/m4/ax_append_flag.m4 build/m4/ax_cflags_warn_all.m4
 build/m4/ax_check_compile_flag.m4 build/m4/ax_compiler_vendor.m4
 build/m4/ax_pthread.m4 build/m4/python.m4 build/m4/python.m4
Copyright: 2008 Guido U. Draheim <guidod@gmx.de>
           2008 Steven G. Johnson <stevenj@alum.mit.edu>
	   2008 Matteo Frigo
           2010 Rhys Ulerich <rhys.ulerich@gmail.com>
	   2011 Daniel Richard G. <skunk@iSKUNK.ORG>
           2011 Maarten Bosmans <mkbosmans@gmail.com>
	   2012, 2013, 2014 Brandon Invergo <brandon@invergo.net>
License: GPL-3+ with Autoconf exception

Files: build/m4/ax_gcc_x86_cpu_supports.m4
Copyright: 2016 Felix Chern <idryman@gmail.com>
           2017 Joseph Benden <joe@benden.us>
License: GPL-2+ with Autoconf exception

Files: build/m4/ax_code_coverage.m4
Copyright: 2012, 2016 Philip Withnall
           2012 Xan Lopez
           2012 Christian Persch
           2012 Paolo Borelli
           2012 Dan Winship
           2015 Bastien ROUCARIES
           2017 Joseph Benden <joe@benden.us>
License: LGPL-2.1+

Files: build/m4/libgcrypt.m4
Copyright: 2002, 2003, 2004, 2011, 2014 g10 Code GmbH
License: Other
 This file is free software; as a special exception the author gives
 unlimited permission to copy and/or distribute it, with or without
 modifications, as long as this notice is preserved.
 .
 This file is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Files: lib/csharp/MonoExample/NDesk-dbus/*
Copyright: 2006-2007 Alp Toker <alp@atoker.com>
License: MIT

Files: lib/csharp/MonoExample/NewStationNotify* lib/csharp/Example1/*
 lib/csharp/WirelessPanda/*
Copyright: 2011 Thomas d'Otreppe
License: BSD-2-clause or LGPL-2.1+

Files: src/makeivs-ng.c src/kstats.c src/aircrack-osdep/linux.c src/aircrack-util/common.c
       src/aircrack-util/common.h src/aircrack-osdep/byteorder.h src/aircrack-osdep/airpcap.c
       src/crypto.c src/aircrack-osdep/common.c src/aircrack-osdep/common.h
Copyright: 2004-2005 Christophe Devine
           2006-2018 Thomas d'Otreppe
License: GPL-2+

Files: src/aircrack-osdep/freebsd.c src/aircrack-osdep/dummy.c src/aircrack-osdep/freebsd_tap.c
       src/aircrack-osdep/openbsd.c src/aircrack-osdep/network.c src/aircrack-osdep/network.h
       src/aircrack-osdep/dummy_tap.c src/aircrack-osdep/cygwin.c src/buddy-ng.c
       src/aircrack-osdep/cygwin.h src/aircrack-osdep/netbsd_tap.c src/aircrack-osdep/cygwin_tap.c
       src/aircrack-osdep/openbsd_tap.c src/aircrack-osdep/linux_tap.c src/aircrack-osdep/osdep.c
       src/aircrack-osdep/osdep.h src/aircrack-osdep/packed.h src/aircrack-osdep/netbsd.c
       src/easside-ng.c src/easside.h src/airserv-ng.c src/besside-ng.c
       src/aircrack-osdep/file.c src/wpaclean.c
Copyright: 2007-2011 Andrea Bittau <a.bittau@cs.ucl.ac.uk>
License: GPL-2+

Files: src/aircrack-osdep/darwin.c src/aircrack-osdep/darwin_tap.c
Copyright: 2009 Kyle Fuller <inbox@kylefuller.co.uk>
License: GPL-2+

Files: src/aircrack-osdep/radiotap/*
Copyright: 2007-2008 Andy Green <andy@warmcat.com>
           2007-2009 Johannes Berg <johannes@sipsolutions.net>
License: GPL-2+

Files: src/aircrack-osdep/radiotap/radiotap.h
Copyright: 2003-2004 David Young
License: BSD-3-clause

Files: src/aircrack-osdep/tap-win32/common.h
Copyright: 2002-2005 OpenVPN Solutions LLC
           2003      Damion K. Wilson
License: GPL-2+

Files: src/crctable.h
Copyright: 2004 KoreK
License: GPL-2+
Comment: This file is extracted from the work published by KoreK
         on 2004 on NetStumbler Forums:
             http://www.netstumbler.org/unix-linux/-t12489.html
         See http://trac.aircrack-ng.org/ticket/953 for details.

Files: src/besside-ng-crawler.c
Copyright: 2010 Pedro Larbig <pedro.larbig@carhs.de>
License: GPL-2+

Files: manpages/*
Copyright: 2006-2014 Thomas d'Otreppe
           2006-2007 Adam Cecile (Le_Vert) <gandalf@le-vert.net>
           2006-2007 Martin Beck
           2008-2011 Rick Farina
           2012      David Francos
           2013      Carlos Alberto Lopez Perez
License: GPL-2+

Files: debian/*
Copyright: 2006-2010 Adam Cecile (Le_Vert) <gandalf@le-vert.net>
           2011-2016 Carlos Alberto Lopez Perez <clopez@igalia.com>
           2016-2018 Samuel Henrique <samueloph@debian.org>
           2017-2018 Sophie Brun <sophie@freexian.com>
           2018      Raphaël Hertzog <hertzog@debian.org>
License: GPL-2+

Files: test/*
Copyright: 2012-2013 Carlos Alberto Lopez Perez <clopez@igalia.com>
License: GPL-2+

Files: src/sha1-git.*
Copyright: 2009 Brandon Casey <drafnel@gmail.com>
           2009 Junio C Hamano <gitster@pobox.com>
           2009 Linus Torvalds <torvalds@linux-foundation.org>
           2009 Nicolas Pitre <nico@cam.org>
           2010 Ramsay Jones <ramsay@ramsay1.demon.co.uk>
           2012 Carlos Alberto Lopez Perez <clopez@igalia.com>
License: GPL-2+

Files: src/gcrypt-openssl-wrapper.h
Copyright: 2012 Carlos Alberto Lopez Perez <clopez@igalia.com>
License: GPL-2+

Files: src/aircrack-util/trampoline*
Copyright: 2018 Joseph Benden <joe@benden.us>
License: GPL-2+

Files: scripts/airdrop-ng/*
Copyright: 2008-2012 Ben Smith <thex1le@gmail.com>
           2012      David Francos Cuartero <xayon@xayon.net>
           2012-2013 Rick Farina <zero_chaos@aircrack-ng.org>
License: GPL-2

Files: scripts/airgraph-ng/*
Copyright: 2008-2012 Ben Smith <thex1le@gmail.com>
           2012      David Francos Cuartero <xayon@xayon.net>
           2012      Rick Farina <zero_chaos@aircrack-ng.org>
License: GPL-2

Files: src/airbase-ng.c src/airtun-ng.c src/tkiptun-ng.c
 src/cowpatty.* src/include/hashcat.h src/aircrack-util/mcs_index_rates.c
 src/aircrack-util/mcs_index_rates.h
Copyright: 2006-2018 Thomas d'Otreppe
           2006-2009 Martin Beck
License: GPL-2+ with OpenSSL exception

Files: src/airolib-ng.c
Copyright: 2007-2009, ebfe
License: GPL-2+ with OpenSSL exception

Files: src/aircrack-ng.c src/aircrack-ng.h src/airdecap-ng.c src/ivstools.c
       src/aireplay-ng.c src/packetforge-ng.c src/airodump-ng.c
       src/airdecloak-ng.c src/airdecloak-ng.h src/version.h src/pcap.h
Copyright: 2001-2005 Christophe Devine
           2006-2010 Thomas d'Otreppe
License: GPL-2+ with OpenSSL exception

Files: src/aircrack-ng.h src/uniqueiv.h
Copyright: 2007-2012 Martin Beck <hirte@aircrack-ng.org>
License: GPL-2+ with OpenSSL exception

Files: src/airventriloquist-ng.c
Copyright: 2015 Tim de Waal
License: GPL-2+ with OpenSSL exception

Files: src/uniqueiv.c
Copyright: 2004-2008 Stanislaw Pusep
License: GPL-2+ with OpenSSL exception

Files: src/wesside-ng.c
Copyright: 2005-2009 Andrea Bittau <a.bittau@cs.ucl.ac.uk>
License: GPL-2+ with OpenSSL exception

Files: src/aircrack-ptw-lib.c src/aircrack-ptw-lib.h
Copyright: 2007-2009 Erik Tews, Andrei Pychkine and Ralf-Philipp Weinmann
License: GPL-2+ with OpenSSL exception

Files: src/aircrack-crypto/sha1-sse2.h
Copyright: nx5 <naplam33@msn.com>
License: GPL-2+ with OpenSSL exception

Files: src/aircrack-crypto/sha1-sse2.S
Copyright: 2008 Alvaro Salmador (naplam33@msn.com)
           2005 Simon Marechal (simon@banquise.net)
License: GPL-2+ with OpenSSL exception

Files: src/wkp-frame.h
Copyright: 2010 ZhaoChunsheng
License: GPL-2+

Files: src/aircrack-util/verifyssid.*
Copyright: 2018 ZhaoChunsheng
License: GPL-2+ with OpenSSL exception

Files: src/include/ethernet.h
Copyright: 1992-2012 The FreeBSD Project
License: BSD-2-clause
Comment: License and authors guessed. This file is extracted from the
         FreeBSD Kernel (src/sys/net/ethernet.h)

Files: src/include/if_arp.h
Copyright: 1986-1993 The Regents of the University of California
License: BSD-3-clause

Files: src/include/ieee80211.h
Copyright: 2001, Atsushi Onoe
           2002-2005 Sam Leffler, Errno Consulting
License: BSD-3-clause

Files: src/include/if_llc.h
Copyright: 1986-1993 The Regents of the University of California
License: BSD-3-clause

Files: src/aircrack-osdep/crctable_osdep.h
Copyright: 1993 Ross N. Williams <ross@guest.adelaide.edu.au>
License: public-domain
 This code is fully public-domain. The crc_tbl_osdep table is generated
 using a well known algorithm published on 1993 by Ross Williams.
 See http://www.ross.net/crc/download/crc_v3.txt for details.

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; under
 version 2 of the License.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+ with OpenSSL exception
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 In addition, as a special exception, the copyright holders give
 permission to link the code of portions of this program with the
 OpenSSL library under certain conditions as described in each
 individual source file, and distribute linked combinations
 including the two.
 You must obey the GNU General Public License in all respects
 for all of the code used other than OpenSSL. If you modify
 file(s) with this exception, you may extend this exception to your
 version of the file(s), but you are not obligated to do so. If you
 do not wish to do so, delete this exception statement from your
 version. If you delete this exception statement from all source
 files in the program, then also delete it here.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * The name of the author may not be used to endorse or promote products
      derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: FSFAP
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved.  This file is offered as-is, without any
 warranty.

License: GPL-2+ with Autoconf exception
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+ with Autoconf exception
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.
